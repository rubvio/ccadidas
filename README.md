# README #
# Adidas coding challenge

The projects within this repository comprise the solution given for a code challenge made for Java. The task consisted of two applications developed following a microservices approach.
One of them would host data in a database and it will expose the content in this DB via a Restful api. The DB contains information about cities and the distances among them.
The second application would also expose a URL (api-2) to the user, so that he/she would be able to introduce to cities. Then the application would use the data provided by 
the first application and based on this data would calculate the shortest path between the given cities. The result would be sent back to the client consuming this 
api (api-2).


### How do I get set up? ###

This repository contains three Maven projects (challenge-api, challenge-best-route and challenge-dbbackend-rest. They are prepared to be built and run 
with Maven and the build sequence has to start with challenge-api, as this is a dependency pointed by the other two projects. The other two projects are independent each other. 

* Maven dependencies needed(artifact ids): 
	+ challenge-api:	
		- jackson-annotations 
	+ challenge-best-route:	
		- spring-boot-starter-web 
		- spring-boot-starter-test 
		- spring-boot-starter-data-rest 
		- junit 
		- commons-lang3 
		- challenge-api  
		- spring-boot-starter-thymeleaf 	
	
	+ challenge-dbbackend-rest:	
		- spring-boot-starter-data-jpa 
		- spring-boot-starter-web 
		- spring-boot-starter-test 
		- h2
		- liquibase-core 
		- spring-boot-starter-data-jpa 
		- spring-boot-starter-data-rest 
		- challenge-api 



* How the applications run: the two running applications are spring boot applications running within a container each of them. challenge-dbbackend-rest runs on the port 8090 and challenge-best-route runs on the port 8080.
* Database configuration: there is no need to configure the DB in challenge-dbbackend-rest. It is a DB in memory, which can be accesed in a browser with the following URL: http://localhost:8090/h2/login.jsp (no user/password are required)
* Both DB configuration (URL, user and password) and server port can be configured in the src/main/resources/application.properties contained in the applications)
* My apps. are running, now what?. The first app. challenge-dbbackend-rest must be running, if you want to do some city path calculations in challenge-best-route.If you want, you can obtain the whole list of connections between cities either by querying directly the DB (see the DB URL above). You can also call the URL within challenge-dbbackend-rest http://localhost:8090/getCityConnections and you will get the json representation of the distances between the cities connected in the DB. This same URL will be called by challenge-best-route afterwards.
* How can I test challenge-best-route?. Easy, you only have to introduce the following URL http://localhost:8080/bestroute/api/distancequery?origin=Valencia&destination=Lugo, the only requirement you should be aware of, is that you have to introduce correctly the parameters origin and destination with a value for both (it does not matter, if the values exist or not in the backend DB, the application should inform you about that)
* Deployment instructions: you only have to take care that challenge-dbbackend-rest is running before triggering any query in challenge-best-route, otherwise the app. will show a error page indicating that the challenge-dbbackend-rest is not running.

### About technologies ###
As stated before, all the technologies used come from the Java ecosystem. The application build and dependencies management is driven by Maven. Spring Boot and Spring are the frameworks used to startup the application and configured the internal components or spring beans. Some features from Spring Web are also used to expose the Rest APIs. The DB in challenge-dbbackend-rest is created and managed by Liquibase, which also can be integrated with Spring. Liquibase offers the possibility to manage all the DDL tasks related with the DB administration. The data with no further configuration inserted in the DB by Spring by reading the script contained in src/main/resources/data.sql. JPA and Hibernate are used to access the DB content from the application side. The mechanism to calculate the  shortest path between two cities was implemented following the Dijkstra algorithm to find the shortest way between nodes within a graph.

### Author ###

* Rub�n Puente