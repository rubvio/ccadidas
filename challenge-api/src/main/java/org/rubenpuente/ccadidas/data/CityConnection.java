package org.rubenpuente.ccadidas.data;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/**
 * Domain model object to hold the information belonging to the connection
 * between two cities
 * 
 * @author Rub�n Puente
 * @year 2018
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class CityConnection implements Serializable {

	private static final long serialVersionUID = -5456449190115203872L;

	private long originId;

	private String originName;

	private long destinationId;

	private String destinationName;

	private int distance;

	public long getOriginId() {
		return originId;
	}

	public void setOriginId(long originId) {
		this.originId = originId;
	}

	public String getOriginName() {
		return originName;
	}

	public void setOriginName(String originName) {
		this.originName = originName;
	}

	public long getDestinationId() {
		return destinationId;
	}

	public void setDestinationId(long destinationId) {
		this.destinationId = destinationId;
	}

	public String getDestinationName() {
		return destinationName;
	}

	public void setDestinationName(String destinationName) {
		this.destinationName = destinationName;
	}

	public int getDistance() {
		return distance;
	}

	public void setDistance(int distance) {
		this.distance = distance;
	}

	@Override
	public String toString() {
		return "CityConnection [originId=" + originId + ", originName=" + originName + ", destinationId="
				+ destinationId + ", destinationName=" + destinationName + ", distance=" + distance + "]";
	}

}
