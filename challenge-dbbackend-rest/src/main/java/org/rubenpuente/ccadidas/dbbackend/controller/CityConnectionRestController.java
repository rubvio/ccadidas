package org.rubenpuente.ccadidas.dbbackend.controller;

import java.util.List;

import org.rubenpuente.ccadidas.data.CityConnection;
import org.rubenpuente.ccadidas.dbbackend.persistence.PersistenceRepositoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * Spring {@link RestController} to expose the URL /getCityConnections, which
 * will return the city connections contained in the application DB.
 * 
 * @author Rubén Puente
 * @year 2018
 */
@RestController
public class CityConnectionRestController {

	@Autowired
	private PersistenceRepositoryService persistenceRepositoryService;

	/**
	 * Queries the internal spring bean connected to the DB to retrieve the
	 * connections between cities.
	 * 
	 * @return {@link java.util.List} containing the {@CityConnection} objects
	 *         retrieved from DB
	 */
	@RequestMapping("/getCityConnections")
	public List<CityConnection> query() {
		return persistenceRepositoryService.findAll();
	}
}