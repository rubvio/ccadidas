package org.rubenpuente.ccadidas.dbbackend;

import javax.sql.DataSource;

import org.rubenpuente.ccadidas.dbbackend.persistence.PersistenceRepositoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import liquibase.integration.spring.SpringLiquibase;

/**
 * Configuration class. Please consider to include here all your beans, so that
 * it can be considered as an entry point for people watching the application
 * 
 * @author Rubén Puente
 * @year 2018
 */
@Configuration
public class ApplicationConfiguration {

	/**
	 * Creates a Bean for handling the DDL DB sentences by leveraging Liquibase for
	 * this task
	 * 
	 * @return
	 */
	@Bean
	public SpringLiquibase liquibase() {
		final SpringLiquibase liquibase = new SpringLiquibase();
		liquibase.setChangeLog("classpath:persistence/changeLog.xml");
		liquibase.setDataSource(dataSource);
		return liquibase;
	}

	/**
	 * Created by spring
	 */
	@Autowired
	private DataSource dataSource;

	@Bean
	public PersistenceRepositoryService connectionTypeRepository() {
		return new PersistenceRepositoryService();
	}
}
