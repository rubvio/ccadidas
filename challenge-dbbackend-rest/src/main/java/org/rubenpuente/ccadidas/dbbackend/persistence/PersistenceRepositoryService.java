package org.rubenpuente.ccadidas.dbbackend.persistence;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.rubenpuente.ccadidas.data.CityConnection;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

@Service
public class PersistenceRepositoryService {

	private static final Logger logger = LoggerFactory.getLogger(PersistenceRepositoryService.class);

	@PersistenceContext
	private EntityManager em;

	public List<CityConnection> findAll() {

		if (logger.isInfoEnabled()) {
			logger.info("Received request to get all city connections");
		}

		final Query q = em.createNativeQuery(
				"SELECT c1.id origin_id, c1.name origin_name, c2.id destination_id, c2.name destination_name, distance FROM city c1 INNER JOIN city_connection ct ON c1.id = ct.city_id_1 INNER JOIN  city c2 ON ct.city_id_2 = c2.id");

		@SuppressWarnings({ "unchecked" })
		final List<Object[]> result = q.getResultList();

		final List<CityConnection> cityConnections = new ArrayList<CityConnection>();
		for (final Object[] a : result) {

			final CityConnection conection = new CityConnection();
			if (a[0] instanceof BigInteger) {
				conection.setOriginId(((BigInteger) a[0]).longValue());
			}
			conection.setOriginName((String) a[1]);

			if (a[2] instanceof BigInteger) {
				conection.setOriginId(((BigInteger) a[2]).longValue());
			}
			conection.setDestinationName((String) a[3]);

			if (a[4] instanceof BigInteger) {
				conection.setDistance(((BigInteger) a[4]).intValue());
			}
			cityConnections.add(conection);
		}

		if (logger.isInfoEnabled()) {
			logger.info("Successfully returning " + cityConnections.size() + " city connections");
		}

		return cityConnections;
	}
}
