
-- Populating city table
INSERT INTO city (id, name) VALUES (1, 'Bilbao');
INSERT INTO city (id, name) VALUES (2, 'Burgos');
INSERT INTO city (id, name) VALUES (3, 'La Rioja');
INSERT INTO city (id, name) VALUES (4, 'Leon');
INSERT INTO city (id, name) VALUES (5, 'Lugo');
INSERT INTO city (id, name) VALUES (6, 'Madrid');
INSERT INTO city (id, name) VALUES (7, 'Oviedo');
INSERT INTO city (id, name) VALUES (8, 'Toledo');
INSERT INTO city (id, name) VALUES (9, 'Zaragoza');
INSERT INTO city (id, name) VALUES (10, 'Valencia');


-- Populating city_connection table
INSERT INTO city_connection (city_id_1, city_id_2, distance) VALUES (1, 2, 111);
INSERT INTO city_connection (city_id_1, city_id_2, distance) VALUES (2, 6, 249);
INSERT INTO city_connection (city_id_1, city_id_2, distance) VALUES (2, 3, 122);
INSERT INTO city_connection (city_id_1, city_id_2, distance) VALUES (3, 9, 192);
INSERT INTO city_connection (city_id_1, city_id_2, distance) VALUES (5, 4, 222);
INSERT INTO city_connection (city_id_1, city_id_2, distance) VALUES (5, 7, 227);
INSERT INTO city_connection (city_id_1, city_id_2, distance) VALUES (4, 2, 187);
INSERT INTO city_connection (city_id_1, city_id_2, distance) VALUES (4, 7, 124);
INSERT INTO city_connection (city_id_1, city_id_2, distance) VALUES (4, 6, 332);
INSERT INTO city_connection (city_id_1, city_id_2, distance) VALUES (1, 7, 282);
INSERT INTO city_connection (city_id_1, city_id_2, distance) VALUES (6, 8, 72);
INSERT INTO city_connection (city_id_1, city_id_2, distance) VALUES (6, 10, 357);
INSERT INTO city_connection (city_id_1, city_id_2, distance) VALUES (9, 6, 314);
INSERT INTO city_connection (city_id_1, city_id_2, distance) VALUES (9, 10, 309);
