package org.rubenpuente.ccadidas.bestroute.service;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;

import org.junit.Test;
import org.rubenpuente.ccadidas.bestroute.data.Node;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class RouteCalculatorTest {

	private final boolean log = false;

	private static final Logger logger = LoggerFactory.getLogger(RouteCalculatorTest.class);

	@Test
	public void testI() {

		final String from = "Leon";
		final String to = "Zaragoza";
		final Map<String, Node> nodes = prepareNodes();
		final RouteCalculator rc = new RouteCalculator();
		final Map<String, Integer> route = rc.calculatePath(nodes.get(from), nodes.get(to));

		print("testI()", from, to, route);
		assertNotNull(route);
		assertEquals(4, route.size());
		final Map<String, Integer> testResult = new LinkedHashMap<String, Integer>();
		testResult.put("Leon", 0);
		testResult.put("Burgos", 187);
		testResult.put("La Rioja", 309);
		testResult.put("Zaragoza", 501);

		assertEquals(testResult, route);
	}

	@Test
	public void testII() {

		final String from = "Valencia";
		final String to = "Lugo";

		final Map<String, Node> nodes = prepareNodes();
		final RouteCalculator rc = new RouteCalculator();
		final Map<String, Integer> route = rc.calculatePath(nodes.get(from), nodes.get(to));

		print("testII()", from, to, route);

		assertNotNull(route);
		assertEquals(4, route.size());
		final Map<String, Integer> testResult = new LinkedHashMap<String, Integer>();
		testResult.put("Valencia", 0);
		testResult.put("Madrid", 357);
		testResult.put("Leon", 689);
		testResult.put("Lugo", 911);

		assertEquals(testResult, route);
	}

	@Test
	public void testIII() {

		final String from = "Valencia";
		final String to = "Valencia";

		final Map<String, Node> nodes = prepareNodes();
		final RouteCalculator rc = new RouteCalculator();
		final Map<String, Integer> route = rc.calculatePath(nodes.get(from), nodes.get(to));

		print("testIII()", from, to, route);

		assertNotNull(route);
		assertEquals(1, route.size());
		final Map<String, Integer> testResult = new LinkedHashMap<String, Integer>();
		testResult.put("Valencia", 0);
		assertEquals(testResult, route);

	}

	@Test
	public void testIV() {

		final String from = "Bilbao";
		final String to = "Toledo";

		final Map<String, Node> nodes = prepareNodes();
		final RouteCalculator rc = new RouteCalculator();
		final Map<String, Integer> route = rc.calculatePath(nodes.get(from), nodes.get(to));

		print("testIV()", from, to, route);

		assertNotNull(route);
		assertEquals(4, route.size());
		final Map<String, Integer> testResult = new LinkedHashMap<String, Integer>();
		testResult.put("Bilbao", 0);
		testResult.put("Burgos", 111);
		testResult.put("Madrid", 360);
		testResult.put("Toledo", 432);
		assertEquals(testResult, route);
	}

	private Map<String, Node> prepareNodes() {
		final Node bil = new Node("Bilbao");
		final Node bur = new Node("Burgos");
		final Node lar = new Node("La Rioja");
		final Node len = new Node("Leon");
		final Node lug = new Node("Lugo");
		final Node mad = new Node("Madrid");
		final Node ovi = new Node("Oviedo");
		final Node tol = new Node("Toledo");
		final Node zar = new Node("Zaragoza");
		final Node val = new Node("Valencia");

		bil.connectTo(bur, 111);

		bur.connectTo(mad, 249);
		bur.connectTo(lar, 122);

		lar.connectTo(zar, 192);
		lug.connectTo(len, 222);
		lug.connectTo(ovi, 227);

		len.connectTo(bur, 187);
		len.connectTo(ovi, 124);
		len.connectTo(mad, 332);

		ovi.connectTo(bil, 282);

		mad.connectTo(tol, 72);
		mad.connectTo(val, 357);

		zar.connectTo(mad, 314);
		zar.connectTo(val, 309);

		final Map<String, Node> nodes = new HashMap<String, Node>();
		nodes.put(bil.getName(), bil);
		nodes.put(bur.getName(), bur);
		nodes.put(lar.getName(), lar);
		nodes.put(len.getName(), len);
		nodes.put(lug.getName(), lug);
		nodes.put(mad.getName(), mad);
		nodes.put(ovi.getName(), ovi);
		nodes.put(tol.getName(), tol);
		nodes.put(val.getName(), val);
		nodes.put(zar.getName(), zar);
		return nodes;
	}

	private void print(String test, String from, String to, Map<String, Integer> route) {
		if (!log) {
			return;
		}
		logger.info("");
		logger.info("--------------------------------------------------");
		logger.info("-------------------- " + test + " -----------------------");
		logger.info("Testing route between " + from + " and " + to + " -> " + route);
	}

}
