package org.rubenpuente.ccadidas.bestroute.controller;

import javax.servlet.RequestDispatcher;
import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.web.servlet.error.ErrorController;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * Handler annotated as {@link Controller} to detect http codes and be able to
 * react to them by showing the corresponding html to the user.
 * 
 * @author Rubén Puente
 * @year 2018
 */
@Controller
public class HttpErrorController implements ErrorController {

	private static final Logger logger = LoggerFactory.getLogger(RequestMappingController.class);

	@RequestMapping("/error")
	public String handleError(HttpServletRequest request) {
		final Object httpStatus = request.getAttribute(RequestDispatcher.ERROR_STATUS_CODE);

		if (httpStatus instanceof Integer) {
			final Integer statusCode = Integer.valueOf(httpStatus.toString());
			if (logger.isWarnEnabled()) {
				logger.warn("Detected http request with status " + httpStatus);
			}
			if (statusCode == HttpStatus.BAD_REQUEST.value()) {
				return "400";
			} else if (statusCode == HttpStatus.NOT_FOUND.value()) {
				return "404";
			} else if (statusCode == HttpStatus.INTERNAL_SERVER_ERROR.value()) {
				return "500";
			}
		}
		return "wrong";
	}

	@Override
	public String getErrorPath() {
		return "/error";
	}
}
