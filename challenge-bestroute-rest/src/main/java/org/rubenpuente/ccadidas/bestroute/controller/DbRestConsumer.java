package org.rubenpuente.ccadidas.bestroute.controller;

import org.rubenpuente.ccadidas.data.CityConnection;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;

/**
 * Defined as spring bean, queries a remote URL configured in
 * application.properties to retrieve an array of {@link CityConnection}
 * 
 * @author Rubén Puente
 * @year 2018
 */
public class DbRestConsumer {

	private static final Logger logger = LoggerFactory.getLogger(DbRestConsumer.class);

	@Value("${org.rubenpuente.ccaddidas.bestroute.dbrest.url}")
	private String dbRestUrl;

	public CityConnection[] getConnections() throws RestClientException {

		if (logger.isInfoEnabled()) {
			logger.info("Requesting remote data from: " + dbRestUrl);
		}

		final RestTemplate restTemplate = new RestTemplate();
		CityConnection[] cc = null;
		try {
			cc = restTemplate.getForObject(dbRestUrl, CityConnection[].class);
		} catch (final RestClientException e) {
			logger.error("There is a problem with the remote server " + dbRestUrl, e);
			throw e;
		}

		if (logger.isInfoEnabled()) {
			logger.info("Result: " + cc);
		}
		return cc;
	}
}
