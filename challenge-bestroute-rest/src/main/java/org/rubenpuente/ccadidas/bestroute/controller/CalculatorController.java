package org.rubenpuente.ccadidas.bestroute.controller;

import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.rubenpuente.ccadidas.bestroute.data.CalculationResult;
import org.rubenpuente.ccadidas.bestroute.data.CalculationResultOk;
import org.rubenpuente.ccadidas.bestroute.data.Node;
import org.rubenpuente.ccadidas.bestroute.service.RouteCalculator;
import org.rubenpuente.ccadidas.bestroute.util.BeanConverter;
import org.rubenpuente.ccadidas.data.CityConnection;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestClientException;

/**
 * Spring service class, which role is to prepare the calculation requested by
 * another component.
 * 
 * @author Rubén Puente
 * @year 2018
 */
@Service
public class CalculatorController {

	private static final Logger logger = LoggerFactory.getLogger(CalculatorController.class);

	@Autowired
	private DbRestConsumer dbRestConsumer;

	public CalculationResult doCalculation(String from, String to) {

		if (StringUtils.isAnyBlank(from, to)) {
			logger.warn("Attempt calculation with empty from or to");
			return new CalculationResult(CalculationResultOk.EMPTY_PARAMS);
		}

		CityConnection[] cc = null;
		try {
			cc = dbRestConsumer.getConnections();
		} catch (final RestClientException e) {

			logger.error("A problem happened while retrieving remote locations");
			return new CalculationResult(CalculationResultOk.REMOTE_ENDPOINT_NOT_AVAILABLE);
		}

		if (cc == null || cc.length == 0) {
			return new CalculationResult(CalculationResultOk.NO_REMOTE_RESULTS);
		}

		if (!exists(cc, from)) {
			return new CalculationResult(CalculationResultOk.NOT_EXISTING_FROM);
		}
		if (!exists(cc, to)) {
			return new CalculationResult(CalculationResultOk.NOT_EXISTING_TO);
		}

		final Map<String, Node> nodes = BeanConverter.convert(cc);

		final Node origin = nodes.get(from);
		final Node destination = nodes.get(to);
		if (origin == null || destination == null) {
			logger.warn("Could not do a right conversion for cities " + from + " and " + to);
			return new CalculationResult(CalculationResultOk.WRONG_CONFIGURATION);
		}
		final RouteCalculator rc = new RouteCalculator();
		final Map<String, Integer> calculation = rc.calculatePath(origin, destination);

		final CalculationResultOk result = new CalculationResultOk();

		result.setTotalDistance(calculation.get(to));

		result.setOrigin(from);
		result.setDestination(to);
		result.setLegs(calculation);
		return result;
	}

	private boolean exists(CityConnection[] cc, String city) {
		for (final CityConnection cityConnection : cc) {
			if (city.equals(cityConnection.getOriginName()) || city.equals(cityConnection.getDestinationName())) {
				return true;
			}
		}
		return false;
	}
}
