package org.rubenpuente.ccadidas.bestroute.controller;

import org.rubenpuente.ccadidas.bestroute.data.CalculationResult;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 * Exposes a URL within the application to get the customer request. It will
 * delegate the responsibility regarding the business object to a
 * {@link CalculatorController}, which result will be shown to the user.
 * 
 * @author Rubén Puente
 * @year 2018
 */
@RestController
public class RequestMappingController {

	private static final Logger logger = LoggerFactory.getLogger(RequestMappingController.class);

	@Autowired
	private CalculatorController calculatorController;

	/**
	 * Exposes the URL /bestroute/api/distancequery with the parameters <i> origin
	 * </i> and <i> destination </i> , which should be populated by the consumer
	 * calling this URL whit a GET request.
	 * 
	 * @param origin      The <i>origin</i> to perform the calculation from
	 * @param destination The <i>origin</i> to compute the shortest distance from
	 *                    <i>origin</i>
	 * @return {@link CalculationResult} object containing the result, or the
	 *         corresponding message within the message returned object
	 * @see CalculationResult
	 */
	@RequestMapping(value = "/bestroute/api/distancequery", params = { "origin",
			"destination" }, method = RequestMethod.GET)
	public CalculationResult query(@RequestParam(value = "origin") String origin,
			@RequestParam(value = "destination") String destination) {

		if (logger.isInfoEnabled()) {
			logger.info("Got request for " + origin + " and " + destination);
		}

		return calculatorController.doCalculation(origin, destination);
	}
}
