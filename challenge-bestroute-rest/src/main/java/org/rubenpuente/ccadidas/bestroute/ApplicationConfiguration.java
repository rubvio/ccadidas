package org.rubenpuente.ccadidas.bestroute;

import org.rubenpuente.ccadidas.bestroute.controller.CalculatorController;
import org.rubenpuente.ccadidas.bestroute.controller.DbRestConsumer;
import org.rubenpuente.ccadidas.bestroute.controller.HttpErrorController;
import org.rubenpuente.ccadidas.bestroute.controller.RequestMappingController;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * Spring configuration class
 * 
 * @author Rubén Puente
 * @year 2018
 */
@Configuration
public class ApplicationConfiguration {

	@Bean
	public DbRestConsumer dbRestConsumer() {
		return new DbRestConsumer();
	}

	@Bean
	public RequestMappingController requestMappingController() {
		return new RequestMappingController();
	}

	@Bean
	public HttpErrorController httpErrorController() {
		return new HttpErrorController();
	}

	@Bean
	public CalculatorController calculatorController() {
		return new CalculatorController();
	}

}
