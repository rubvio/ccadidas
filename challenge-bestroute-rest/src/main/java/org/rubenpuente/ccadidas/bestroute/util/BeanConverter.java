package org.rubenpuente.ccadidas.bestroute.util;

import java.util.HashMap;
import java.util.Map;

import org.rubenpuente.ccadidas.bestroute.data.Node;
import org.rubenpuente.ccadidas.data.CityConnection;

/**
 * Utility class to help in the conversion tasks between the entry coming from
 * the remote endpoint delivering {@see CityConnection} and the
 * {@org.rubenpuente.ccadidas.bestroute.data.Node} used within this project.
 * 
 * @author Rubén Puente
 * @year 2018
 */
public class BeanConverter {

	public static Map<String, Node> convert(CityConnection[] connections) {
		final Map<String, Node> result = new HashMap<String, Node>();
		for (final CityConnection connection : connections) {

			Node from = result.get(connection.getOriginName());
			if (from == null) {
				from = new Node(connection.getOriginName());
				result.put(connection.getOriginName(), from);
			}

			Node to = result.get(connection.getDestinationName());
			if (to == null) {
				to = new Node(connection.getDestinationName());
				result.put(connection.getDestinationName(), to);
			}

			from.connectTo(to, connection.getDistance());
		}
		return result;
	}

}
