package org.rubenpuente.ccadidas.bestroute.data;

import org.rubenpuente.ccadidas.bestroute.controller.CalculatorController;
import org.rubenpuente.ccadidas.bestroute.controller.RequestMappingController;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

/**
 * POJO to use as the result for a call to
 * {@link CalculatorController#doCalculation()}. It will also be sent to the
 * client calling {@link RequestMappingController}
 * 
 * @author Rubén Puente
 * @year 2018
 */
@JsonInclude(Include.NON_NULL)
public class CalculationResult {

	public static final int EMPTY_PARAMS = -1;
	public static final int WRONG_CONFIGURATION = -2;
	public static final int NOT_EXISTING_FROM = -3;
	public static final int NOT_EXISTING_TO = -4;
	public static final int REMOTE_ENDPOINT_NOT_AVAILABLE = -5;
	public static final int NO_REMOTE_RESULTS = -6;
	public static final int OK = 0;

	public CalculationResult(int resultCode) {
		this.resultCode = resultCode;
		switch (resultCode) {
		case CalculationResult.EMPTY_PARAMS:
			this.message = "Origin and destination are mandatory params";
			break;

		case CalculationResult.WRONG_CONFIGURATION:
			this.message = "An error happened during the calculation configuration phase";
			break;

		case CalculationResult.NOT_EXISTING_FROM:
			this.message = "Origin does not exist in the service";
			break;

		case CalculationResult.NOT_EXISTING_TO:
			this.message = "Destination does not exist in the service";
			break;

		case CalculationResult.REMOTE_ENDPOINT_NOT_AVAILABLE:
			this.message = "Remote endpoint is not avaliable";
			break;

		case CalculationResult.NO_REMOTE_RESULTS:
			this.message = "The remote city-location service did not give back any result";
			break;

		default:
			break;
		}
	}

	private String message;

	private final int resultCode;

	public String getMessage() {
		return message;
	}

	@JsonIgnore
	public int getResultCode() {
		return resultCode;
	}

	@Override
	public String toString() {
		return "CalculationResult [message=" + message + ", resultCode=" + resultCode + "]";
	}

}
