package org.rubenpuente.ccadidas.bestroute.data;

import java.util.Map;

/**
 * POJO representing a successful execution of this application logic (i.e. the
 * information regarding the calculation between an origin and a destination, it
 * also includes the legs, if any, between the two locations.
 * 
 * @author Rubén Puente
 * @year 2018
 */
public class CalculationResultOk extends CalculationResult {

	public CalculationResultOk() {
		super(CalculationResult.OK);
	}

	private String origin;

	private String destination;

	private int totalDistance;

	private Map<String, Integer> legs;

	public String getOrigin() {
		return origin;
	}

	public void setOrigin(String origin) {
		this.origin = origin;
	}

	public String getDestination() {
		return destination;
	}

	public void setDestination(String destination) {
		this.destination = destination;
	}

	public int getTotalDistance() {
		return totalDistance;
	}

	public void setTotalDistance(int totalDistance) {
		this.totalDistance = totalDistance;
	}

	public Map<String, Integer> getLegs() {
		return legs;
	}

	public void setLegs(Map<String, Integer> legs) {
		this.legs = legs;
	}

	@Override
	public String toString() {
		return "CalculationResultOk [origin=" + origin + ", destination=" + destination + ", totalDistance="
				+ totalDistance + ", legs=" + legs + "]";
	}

}
