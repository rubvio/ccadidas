package org.rubenpuente.ccadidas.bestroute.data;

import java.util.ArrayList;
import java.util.List;

/**
 * Represents one location or city and the cities connected to it.
 * 
 * @author Rubén Puente
 * @year 2018
 */
public class Node implements Comparable<Node> {

	private final String name;

	private final List<Neighboor> neighbors;

	private int distance = Integer.MAX_VALUE;

	private Node nearestNode;

	private boolean visited;

	public Node(String name) {
		this.name = name;
		this.neighbors = new ArrayList<Neighboor>();
	}

	public String getName() {
		return name;
	}

	public Node getNearestNode() {
		return nearestNode;
	}

	public void setNearestNode(Node nearestNode) {
		this.nearestNode = nearestNode;
	}

	public int getDistance() {
		return distance;
	}

	public void setDistance(int distance) {
		this.distance = distance;
	}

	public void addNeighboor(Neighboor n) {
		if (n == null || neighbors.contains(n)) {
			return;
		}
		neighbors.add(n);
	}

	public void connectTo(Node other, int distance) {
		this.addNeighboor(new Neighboor(other, distance));
		other.addNeighboor(new Neighboor(this, distance));
	}

	public List<Neighboor> getNeighbors() {
		return neighbors;
	}

	public boolean isVisited() {
		return visited;
	}

	public void setVisited(boolean visited) {
		this.visited = visited;
	}

	@Override
	public int compareTo(Node other) {
		return Long.compare(distance, other.distance);
	}

	@Override
	public String toString() {
		return "Node [name=" + name + "]";
	}

}