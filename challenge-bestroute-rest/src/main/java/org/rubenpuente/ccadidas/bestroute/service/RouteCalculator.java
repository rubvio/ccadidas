package org.rubenpuente.ccadidas.bestroute.service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.ListIterator;
import java.util.Map;
import java.util.PriorityQueue;

import org.rubenpuente.ccadidas.bestroute.data.Neighboor;
import org.rubenpuente.ccadidas.bestroute.data.Node;

/**
 * Responsible for computing the shortest distance between two {@link Node}. It
 * uses an implementation of Dijkstra's Algorithm to find the shortest distance
 * between two nodes.
 * 
 * @author Rubén Puente
 * @year 2018
 */
public class RouteCalculator {

	public Map<String, Integer> calculatePath(Node source, Node destination) {

		if (source == null || destination == null) {
			return null;
		}

		source.setDistance(0);

		final List<Node> result = new ArrayList<Node>();
		if (source.getName().equals(destination.getName())) {
			result.add(source);
			final Map<String, Integer> r = new HashMap<String, Integer>();
			r.put(source.getName(), 0);
			return r;
		}

		final PriorityQueue<Node> queue = new PriorityQueue<Node>();
		queue.add(source);

		while (!queue.isEmpty()) {
			final Node node = queue.poll();
			node.setVisited(true);

			for (final Neighboor neighboor : node.getNeighbors()) {

				final Node parent = neighboor.getParent();
				if (parent.isVisited()) {
					continue;
				}
				final int neighboorDistance = neighboor.getDistance();

				if (node.getDistance() + neighboorDistance < parent.getDistance()) {
					queue.remove(parent);
					neighboor.setDistance(neighboorDistance);
					parent.setDistance(node.getDistance() + neighboorDistance);
					parent.setNearestNode(node);
					queue.add(parent);
				}
			}
		}

		return getPathTo(destination);
	}

	/**
	 * Internal method to iterate over the shortest path between the destination and
	 * the origin
	 * 
	 * @param destination {@link Node} from which the method will calculate the
	 *                    shortest distance to the origin
	 * @return {@link java.util.Map}, which entries represent the legs between
	 *         origin and destination, with the accumulated distance for a certain
	 *         leg from the beginning.
	 */
	private Map<String, Integer> getPathTo(Node destination) {

		final Map<String, Integer> tmp = new LinkedHashMap<String, Integer>();

		Node node = destination;
		while (node != null) {
			tmp.put(node.getName(), node.getDistance());
			node = node.getNearestNode();
		}
		final ListIterator<String> it = new ArrayList<String>(tmp.keySet()).listIterator(tmp.size());
		final Map<String, Integer> result = new LinkedHashMap<String, Integer>();
		while (it.hasPrevious()) {
			final String key = it.previous();
			result.put(key, tmp.get(key));
		}
		return result;
	}
}
