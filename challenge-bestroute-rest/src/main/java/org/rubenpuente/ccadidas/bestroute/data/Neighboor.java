package org.rubenpuente.ccadidas.bestroute.data;

/**
 * Link between a {@Link org.rubenpuente.ccadidas.bestroute.data.Node} and the
 * other cities directly connected to it. It is a help object to be used to find
 * out, if the distance between two nodes not connected directly is better than
 * the previous calculated distance
 * 
 * @author Rubén Puente
 * @year 2018
 */
public class Neighboor {

	private final Node parent;

	private int distance;

	public Neighboor(Node other, int distance) {
		this.parent = other;
		this.distance = distance;
	}

	public int getDistance() {
		return distance;
	}

	public void setDistance(int distance) {
		this.distance = distance;
	}

	public Node getParent() {
		return parent;
	}

	@Override
	public String toString() {
		return "Neighboor [parent=" + parent.getName() + ", distance=" + distance + "]";
	}

}